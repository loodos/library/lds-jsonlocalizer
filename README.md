# LDS.JsonLocalizer

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg?style=flat-square)](https://gitlab.com/loodos/library/lds-jsonlocalizer/blob/master/LICENSE)

[![dev build status](https://gitlab.com/loodos/library/lds-jsonlocalizer/badges/dev/build.svg)](https://gitlab.com/loodos/library/lds-jsonlocalizer/commits/dev)
[![master build status](https://gitlab.com/loodos/library/lds-jsonlocalizer/badges/master/build.svg)](https://gitlab.com/loodos/library/lds-jsonlocalizer/commits/master)

[![NuGet Pre Release](https://img.shields.io/nuget/v/LDS.JsonLocalizer.svg)](https://www.nuget.org/packages/LDS.JsonLocalizer/)
[![NuGet Pre Release](https://img.shields.io/nuget/vpre/LDS.JsonLocalizer.svg)](https://www.nuget.org/packages/LDS.JsonLocalizer/)

## What is it

Localization with json files.

## Using the library

//TODO: write description
services.AddJsonLocalization(options => options.JsonResourcesPath = "json-resources");

### Configuration

//TODO: write description

## Full Sample

//TODO: write description

## Who is using this

//TODO: add projects which is using this

## Commercial Support

//TODO: write description

## Contributing

//TODO: write description

## License

LDS.JsonLocalizer is release under Apache 2.0 License.

Copyright (c) 2018 [Loodos](https://loodos.com)

See [LICENSE](https://gitlab.com/loodos/library/lds-jsonlocalizer/blob/master/LICENSE)

## Acknowledgements

- [ASP.NET Core](https://github.com/aspnet)

Thanks for providing free open source licensing

- 
