﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace LDS.JsonLocalizer
{
    // Custom IStringLocalizerFactory
    // https://github.com/aspnet/Localization/blob/43b974482c7b703c92085c6f68b3b23d8fe32720/src/Microsoft.Extensions.Localization/ResourceManagerStringLocalizerFactory.cs

    public class JsonStringLocalizerFactory : IStringLocalizerFactory
    {
        private readonly ConcurrentDictionary<string, JsonStringLocalizer> _localizerCache = new ConcurrentDictionary<string, JsonStringLocalizer>();

        private readonly string _jsonResourcesPath;
        private readonly ILogger<JsonStringLocalizerFactory> _logger;
        private readonly ILoggerFactory _loggerFactory;

        public JsonStringLocalizerFactory(IOptions<JsonLocalizationOptions> jsonLocalizationOptions, ILogger<JsonStringLocalizerFactory> logger, ILoggerFactory loggerFactory)
        {
            if (jsonLocalizationOptions == null)
            {
                throw new ArgumentNullException(nameof(jsonLocalizationOptions));
            }

            _jsonResourcesPath = jsonLocalizationOptions.Value.JsonResourcesPath ?? string.Empty;
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _loggerFactory = loggerFactory ?? throw new ArgumentNullException(nameof(loggerFactory));

            _logger.LogDebug($"Created {nameof(JsonStringLocalizerFactory)} with json resources path:{_jsonResourcesPath}");
        }

        public IStringLocalizer Create(Type resourceSource)
        {
            if (resourceSource == null)
            {
                throw new ArgumentNullException(nameof(resourceSource));
            }

            var typeInfo = resourceSource.GetTypeInfo();
            var assembly = typeInfo.Assembly;


            var resourcesPath = _jsonResourcesPath;

            if (!_jsonResourcesPath.StartsWith(Path.DirectorySeparatorChar))
            {
                var applicationRootPath = new FileInfo(assembly.Location).DirectoryName;
                resourcesPath = Path.Combine(applicationRootPath, resourcesPath);
            }

            _logger.LogDebug($"Getting JsonStringLocalizer for type: {typeInfo.FullName}");

            return _localizerCache.GetOrAdd(typeInfo.FullName, _ => CreateJsonStringLocalizer(resourcesPath, typeInfo.FullName));
        }

        public IStringLocalizer Create(string baseName, string location)
        {
            if (baseName == null)
            {
                throw new ArgumentNullException(nameof(baseName));
            }

            if (location == null)
            {
                throw new ArgumentNullException(nameof(location));
            }

            _logger.LogDebug($"Getting JsonStringLocalizer for baseName {baseName} and location {location}");

            return _localizerCache.GetOrAdd($"B={baseName},L={location}", _ => CreateJsonStringLocalizer(location, baseName));
        }

        protected virtual JsonStringLocalizer CreateJsonStringLocalizer(string resourcesPath, string resourceName)
        {
            _logger.LogDebug($"Create JsonStringLocalizer for resource {resourceName} and location {resourcesPath}");

            return new JsonStringLocalizer(
                resourcesPath,
                resourceName,
                _loggerFactory.CreateLogger<JsonStringLocalizer>());
        }
    }
}
