﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.JsonLocalizer
{
    public class JsonLocalizationOptions
    {
        public string JsonResourcesPath { get; set; }
    }
}
