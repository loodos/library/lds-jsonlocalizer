﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LDS.JsonLocalizer.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace LDS.JsonLocalizer
{
    // https://github.com/aspnet/Localization/blob/43b974482c7b703c92085c6f68b3b23d8fe32720/src/Microsoft.Extensions.Localization/ResourceManagerStringLocalizer.cs

    public class JsonStringLocalizer : IStringLocalizer
    {
        private readonly ConcurrentDictionary<string, IEnumerable<KeyValuePair<string, string>>> _jsonResourcesCache = new ConcurrentDictionary<string, IEnumerable<KeyValuePair<string, string>>>();
        private readonly ConcurrentDictionary<string, object> _missingManifestCache = new ConcurrentDictionary<string, object>();
        private readonly string _resourcesPath;
        private readonly string _resourceName;
        private readonly ILogger _logger;

        private string _searchedLocation;

        public JsonStringLocalizer(string resourcesPath, string resourceName, ILogger logger)
        {
            _resourcesPath = resourcesPath ?? throw new ArgumentNullException(nameof(resourcesPath));
            _resourceName = resourceName ?? throw new ArgumentNullException(nameof(resourceName));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            _logger.LogDebug($"Creating JsonStringLocalizer for resource {resourceName} and location {resourcesPath}");
        }
        
        public LocalizedString this[string name]
        {
            get
            {
                if (name == null)
                {
                    throw new ArgumentNullException(nameof(name));
                }

                var value = GetStringSafely(name);

                return new LocalizedString(name, value ?? name, resourceNotFound: value == null, searchedLocation: _searchedLocation);
            }
        }

        public LocalizedString this[string name, params object[] arguments]
        {
            get
            {
                if (name == null)
                {
                    throw new ArgumentNullException(nameof(name));
                }

                var format = GetStringSafely(name);
                var value = string.Format(format ?? name, arguments);

                return new LocalizedString(name, value, resourceNotFound: format == null, searchedLocation: _searchedLocation);
            }
        }

        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures) =>
           GetAllStrings(includeParentCultures, CultureInfo.CurrentUICulture);

        protected IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures, CultureInfo culture)
        {
            if (culture == null)
            {
                throw new ArgumentNullException(nameof(culture));
            }

            throw new NotImplementedException();
        }

        public IStringLocalizer WithCulture(CultureInfo culture)
        {
            if (culture == null)
            {
                return new JsonStringLocalizer(_resourcesPath, _resourceName, _logger);
            }
            throw new NotImplementedException();
        }

        private string[] GetCultureSuffixes(CultureInfo culture)
        {
            // Get ordered culture suffixes (e.g.: { ".tr-TR", ".tr", "" }).
            string[] cultureSuffixes;
            if (culture == null)
            {
                cultureSuffixes = new[] { "" };
            }
            else
            {
                if (culture.IsNeutralCulture)
                {
                    cultureSuffixes = new[] { $".{culture.Name}", "" };
                }
                else
                {
                    cultureSuffixes = new[] { $".{culture.Name}", $".{culture.Parent.Name}", "" };
                }
            }

            var cultureSuffixesLogString = string.Join(", ", cultureSuffixes);
            _logger.LogDebug($"Using culture suffixes {cultureSuffixesLogString}");
            return cultureSuffixes;
        }

        protected string FindResourceFile(CultureInfo culture)
        {
            foreach (var cultureSuffix in GetCultureSuffixes(culture))
            {
                _logger.LogDebug($"Using culture suffixes: {cultureSuffix}");

                var filePath = FindResourceFileRecursive(_resourcesPath, _resourceName, cultureSuffix);

                if (!string.IsNullOrEmpty(filePath))
                {
                    return filePath;
                }
            }

            return null;
        }

        protected string FindResourceFileRecursive(string path, string name, string cultureSuffix)
        {
            // try find in root level
            var fileName = name + cultureSuffix + ".json";
            var filePath = Path.Combine(path, fileName);

            if (File.Exists(filePath))
            {
                return filePath;
            }
            _logger.LogDebug($"Resource file location {filePath} does not exist");

            // Start replacing periods, starting at the beginning.
            var segmentCount = name.Count(ch => ch == '.');
            var indexDot = 0;

            // try find in subfolders
            for (var i = 0; i < segmentCount; i++)
            {
                var partFolder = name.Substring(0, indexDot = name.IndexOf('.', indexDot));
                var partFile = name.Substring(indexDot + 1);

                filePath = FindResourceFileRecursive(Path.Combine(path, partFolder), partFile, cultureSuffix);

                if (!string.IsNullOrEmpty(filePath))
                {
                    return filePath;
                }

                indexDot++;
            }

            return null;
        }

        protected string GetStringSafely(string name, CultureInfo culture = null)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            var keyCulture = culture ?? CultureInfo.CurrentUICulture;

            _logger.SearchedLocation(name, _searchedLocation, keyCulture);

            var missingManifestCacheKey = $"name={name}&culture={keyCulture.Name}";
            if (_missingManifestCache.ContainsKey(missingManifestCacheKey))
            {
                return null;
            }

            var resources = _jsonResourcesCache.GetOrAdd(keyCulture.Name, _ =>
            {
                IEnumerable<KeyValuePair<string, string>> value = null;

                _logger.LogDebug($"Getting json resource file for resource {_resourceName} and location {_resourcesPath}");

                var resourceFile = FindResourceFile(keyCulture);

                if (!string.IsNullOrWhiteSpace(resourceFile))
                {
                    _logger.LogDebug($"Json resource file found for resource {_resourceName} and location {resourceFile}");

                    _searchedLocation = resourceFile;

                    var builder = new ConfigurationBuilder()
                    .SetBasePath(_resourcesPath)
                    .AddJsonFile(resourceFile.Replace(_resourcesPath, ""), optional: false, reloadOnChange: true);

                    var config = builder.Build();
                    value = config.AsEnumerable();
                }
                else
                {
                    _logger.LogInformation($"No resource file found or error occurred for base name {_resourceName}, culture {keyCulture} and key '{name}'");
                }

                return value;
            });


            var resource = resources?.SingleOrDefault(s => s.Key == name);

            var resourceValue = resource?.Value ?? null;

            if (resourceValue == null)
            {
                _missingManifestCache.TryAdd(missingManifestCacheKey, null);
            }

            return resourceValue;
        }
    }
}
